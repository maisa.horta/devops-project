package br.puc.devops.calculadora;

public class Calculadora {
    //Método que executa a multiplicação entre 2 números
	public Double multiplica(double d, double e) {
		return d * e;
	}
    //Método para Calcular a raiz cúbica de um número
	public Double raizCubica(double i) {
		return Math.cbrt(i);
	}
    //Método para calcular a potência de um número por outro
	public Double potencia(double base, double expoente) {
		return Math.pow(base, expoente);
	}

}
