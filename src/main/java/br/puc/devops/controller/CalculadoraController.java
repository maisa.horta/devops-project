package br.puc.devops.controller;

import br.puc.devops.calculadora.Calculadora;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculadoraController {

	@RequestMapping("/")
	public String welcomeAPI() {
		return "<p> Bem vindo à calculadora online, por favor utilize valores em double. </p>" + 
			"<p> Exemplo de utilização multiplicação: /multiplica?fator1=8&fator2=2 </p>" +
			"<p> Exemplo de utilização raizCubica: /raizcubica?numero=8 </p>" + 
			"<p> Exemplo para cálculo da potência : /potencia?base=3&expoente=2 </p>";
	}

	@RequestMapping("/multiplica")
	public String multiplica(@RequestParam(value = "fator1") String fator1, @RequestParam(value = "fator2") String fator2) {

		Calculadora calc = new Calculadora();
		Double resposta = calc.multiplica(new Double(fator1), new Double(fator2));
		return resposta.toString();
	}

	@RequestMapping("/raizcubica")
	public String raizCubica(@RequestParam(value = "numero") String numero) {

		Calculadora calc = new Calculadora();
		Double resposta = calc.raizCubica(new Double(numero));
		return resposta.toString();
	}
	
	@RequestMapping("/potencia")
	public String potencia(@RequestParam(value = "base") String base, @RequestParam(value = "expoente") String expoente) {

		Calculadora calc = new Calculadora();
		Double resposta = calc.potencia(new Double(base), new Double(expoente));
		return resposta.toString();
	}

}
