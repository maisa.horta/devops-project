package com.example.demo;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import br.puc.devops.calculadora.Calculadora;

public class CalculadoraTest {

	@Test
	public void primeiroTeste() {
		Calculadora calc = new Calculadora();
		
		Double multiplicacao = calc.multiplica(2.0, 2.0);
		Assert.assertEquals(new Double(4.0), multiplicacao);
	}
	
	@Test
	public void multiplicacaoPorZero() {
		Calculadora calc = new Calculadora();
		
		Double multiplicacao = calc.multiplica(0, 2.0);
		Assert.assertEquals(new Double(0), multiplicacao);
	}
	
	@Test
	public void multiplicacaoProdutoNegativo() {
		Calculadora calc = new Calculadora();
		
		Double multiplicacao = calc.multiplica(2.0, -2.0);
		Assert.assertEquals(new Double(-4.0), multiplicacao);
	}

	@Test
	public void raizCubica() {
		Calculadora calc = new Calculadora();
		
		Double raiz3 = calc.raizCubica(8.0);
		Assert.assertEquals(new Double(2.0), raiz3);
	}
	
	@Test
	public void raizCubicaNegativo() {
		Calculadora calc = new Calculadora();
		
		Double raiz3 = calc.raizCubica(-8.0);
		Assert.assertEquals(new Double(-2.0), raiz3);
	}
	
	@Test
	public void raizCubicaZero() {
		Calculadora calc = new Calculadora();
		
		Double raiz3 = calc.raizCubica(0.0);
		Assert.assertEquals(new Double(0.0), raiz3);
	}
	
	public void outraPotenciaZero() {
		Calculadora calc = new Calculadora();
		
		Double potencia = calc.potencia(10.0, 0.0);
		Assert.assertEquals(new Double(1.0), potencia);
	}
	
	@Test
	public void potenciaUm() {
		Calculadora calc = new Calculadora();
		
		Double potencia = calc.potencia(20.0, 1.0);
		Assert.assertEquals(new Double(20.0), potencia);
	}
	
	
	@Test
	public void potenciaDois() {
		Calculadora calc = new Calculadora();
		
		Double potencia = calc.potencia(5.0, 2.0);
		Assert.assertEquals(new Double(25.0), potencia);
	}
	
	@Test
	public void potenciaFracionada() {
		Calculadora calc = new Calculadora();
		
		Double potencia = calc.potencia(9.0, 0.5);
		Assert.assertEquals(new Double(3.0), potencia);
	}

}
